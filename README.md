# README #

An application requires JDK 8 (usage of a new Stream API) to run and Gradle to build.

To build an application:

```
#!

$ gradle build
```

To run an application:

```
#!

$ java -jar robotswalkjava-1.0.jar
```

To run tests:

```
#!

$ gradle test
```
