package com.dpokidov.robotswalk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.dpokidov.robotswalk.dsl.CommandExecutor;
import com.dpokidov.robotswalk.dsl.Parser;
import com.dpokidov.robotswalk.model.Robot;

/**
 * @author DPokidov
 */
public class App {
    private static final Logger log = Logger.getLogger(App.class.getName());

    /**
     * Chooses input source from file or stdin and run the parser to read and executes commands.
     * The first argument represents a file name to read commands. 
     * If the first argument is empty or file does not exist then stdin will be used as input. 
     * @param args
     */
    public static void main(String[] args) {
        RobotController controller = new RobotController(new Robot());
        try {
            LogManager.getLogManager().readConfiguration(App.class.getResourceAsStream("/logging.properties"));
            Parser parser = new Parser(selectInput(args), new CommandExecutor(controller));
            parser.start();
        } catch (IOException e) {
            log.log(Level.SEVERE, "Error while executing", e);
        }
    }

    private static BufferedReader selectInput(String[] args) {
        BufferedReader input = null;
        if (args.length > 0) {
            try {
                input = Files.newBufferedReader(Paths.get(args[0]));
            } catch (Exception e) {
                log.log(Level.SEVERE, "Can not open a file", e);
            }
        }
        if (input == null) {
            input = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Let's start walking. Please enter a 'place' command first");
        }
        return input;
    }
}
