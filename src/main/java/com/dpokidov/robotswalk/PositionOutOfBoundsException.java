package com.dpokidov.robotswalk;

@SuppressWarnings("serial")
public class PositionOutOfBoundsException extends Exception {

    public PositionOutOfBoundsException() {
        super();
    }

    public PositionOutOfBoundsException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public PositionOutOfBoundsException(String message, Throwable cause) {
        super(message, cause);
    }

    public PositionOutOfBoundsException(String message) {
        super(message);
    }

    public PositionOutOfBoundsException(Throwable cause) {
        super(cause);
    }
}
