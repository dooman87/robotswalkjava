package com.dpokidov.robotswalk;

import static com.dpokidov.robotswalk.model.Direction.*;

import com.dpokidov.robotswalk.dsl.Command;
import com.dpokidov.robotswalk.model.Direction;
import com.dpokidov.robotswalk.model.Robot;

/**
 * This class represents a semantic model for DSL.
 * Each command that can be executed should have {@code Command} annotation.
 * 
 * @author dooman
 *
 */
public class RobotController {
    private static final int MAX_X = 4;
    private static final int MAX_Y = 4;

    private Robot robot;

    public RobotController(Robot robot) {
        this.robot = robot;
    }

    /**
     * Places robot to a new position if position in bound.
     * @param x new x position, should be {@code>= 0}
     * @param y new y position, should be {@code>= 0}
     * @param direction new robot facing, can not be {@code null}
     * @throws IllegalArgumentException if direction == null
     * @throws PositionOutOfBoundsException if new position is invalid (x or y < 0) or is out of bound
     */
    @Command
    public void place(int x, int y, Direction direction) throws IllegalArgumentException, PositionOutOfBoundsException {
        if (direction == null) {
            throw new IllegalArgumentException("Direction must be set");
        }
        checkBounds(x, y);

        robot.placed();
        robot.setPosition(x, y);
        robot.setDirection(direction);
    }

    /**
     * Moves a robot forward if new position is valid.
     * @throws RobotNotPlacedException if the robot have not been placed yet
     * @throws PositionOutOfBoundsException if new position is invalid (x or y < 0) or is out of bound
     */
    @Command
    public void move() throws RobotNotPlacedException, PositionOutOfBoundsException {
        checkPlaced();
        int newX = robot.getX();
        int newY = robot.getY();

        switch (robot.getDirection()) {
            case NORTH: newY++; break;
            case WEST:  newX--; break;
            case EAST:  newX++; break;
            case SOUTH: newY--; break;
        }
        checkBounds(newX, newY);

        robot.setPosition(newX, newY);
    }

    /**
     * Turns a robot left.
     * @throws RobotNotPlacedException if the robot have not been placed yet
     */
    @Command
    public void left() throws RobotNotPlacedException {
        checkPlaced();
        Direction newDirection = robot.getDirection();
        switch (robot.getDirection()) {
            case EAST:  newDirection = NORTH; break;
            case NORTH: newDirection = WEST; break;
            case SOUTH: newDirection = EAST; break;
            case WEST:  newDirection = SOUTH; break;
        }
        robot.setDirection(newDirection);
    }

    /**
     * Turns a robot right.
     * @throws RobotNotPlacedException if the robot have not been placed yet
     */
    @Command
    public void right() throws RobotNotPlacedException {
        checkPlaced();
        Direction newDirection = robot.getDirection();
        switch (robot.getDirection()) {
            case EAST:  newDirection = SOUTH; break;
            case NORTH: newDirection = EAST; break;
            case SOUTH: newDirection = WEST; break;
            case WEST:  newDirection = NORTH; break;
        }
        robot.setDirection(newDirection);
    }

    /**
     * Prints current state (position and direction) of a robot.
     * @throws RobotNotPlacedException if the robot have not been placed yet
     */
    @Command
    public void report() throws RobotNotPlacedException {
        checkPlaced();
        System.out.println(robot.getX() + " " + robot.getY() + " " + robot.getDirection().name());
    }

    private void checkPlaced() throws RobotNotPlacedException {
        if (!robot.isPlaced()) {
            throw new RobotNotPlacedException();
        }
    }

    private void checkBounds(int x, int y) throws PositionOutOfBoundsException {
        if (x < 0 || y < 0 || x > MAX_X || y > MAX_Y) {
            throw new PositionOutOfBoundsException("Position is out of bound, valid position should be in range from 0 to " + MAX_X);
        }
    }
}
