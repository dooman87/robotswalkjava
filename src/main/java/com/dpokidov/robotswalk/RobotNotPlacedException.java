package com.dpokidov.robotswalk;

@SuppressWarnings("serial")
public class RobotNotPlacedException extends Exception {

    public RobotNotPlacedException() {
        super();
    }

    public RobotNotPlacedException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public RobotNotPlacedException(String message, Throwable cause) {
        super(message, cause);
    }

    public RobotNotPlacedException(String message) {
        super(message);
    }

    public RobotNotPlacedException(Throwable cause) {
        super(cause);
    }
}
