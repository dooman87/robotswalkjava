package com.dpokidov.robotswalk.dsl;

@SuppressWarnings("serial")
public class CommandExecutionException extends RuntimeException {

    public CommandExecutionException() {
        super();
    }

    public CommandExecutionException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public CommandExecutionException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandExecutionException(String message) {
        super(message);
    }

    public CommandExecutionException(Throwable cause) {
        super(cause);
    }
}
