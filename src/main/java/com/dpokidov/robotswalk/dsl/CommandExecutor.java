package com.dpokidov.robotswalk.dsl;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is used to execute commands ({@link Command}) which were delivered by {@link Parser}.
 * Commands can be executed on any target object which was passed to constructor.
 * 
 * @author dooman
 *  
 */
public class CommandExecutor {
    private final Logger log = Logger.getLogger(CommandExecutor.class.getName());

    static class CommandDesc {
        public Method method;
        public List<Class<?>> args;
    }

    private Object target;
    private Map<String, CommandDesc> commands;

    public CommandExecutor(Object target) {
        super();
        this.target = target;
        introspectTarget();
    }

    private void introspectTarget() {
        commands = new HashMap<>();

        Class<?> targetClass = target.getClass();
        for (Method method : targetClass.getDeclaredMethods()) {
            if (method.getAnnotation(Command.class) != null) {
                if (log.isLoggable(Level.FINEST)) {
                    log.finest("Add command: [" + method.getName() + "]");
                }
                CommandDesc desc = new CommandDesc();
                desc.method = method;
                desc.args = Arrays.asList(method.getParameterTypes());
                commands.put(method.getName(), desc);
            }
        }
    }

    /**
     * <p>
     * Executes a command on a target object.
     * </p>
     * <p>
     * {@code commandSpec} is an array of strings already prepared by a parser without any spaces.
     * The first string is a command name, the rest of strings are arguments for the command.
     * </p>
     * @param commandSpec array of strings which represents a command. The first string is a command name, others are arguments
     * @throws CommandNotFoundException if a command does not exists
     * @throws CommandExecutionException if any errors occur in arguments convertation or a command execution
     */
    public void execute(String... commandSpec) throws CommandExecutionException, CommandNotFoundException {
        String commandName = commandSpec[0];
        CommandDesc command = commands.get(commandName.toLowerCase());
        if (command == null) {
            throw new CommandNotFoundException("Command [" + commandName + "] not found");
        }
        try {
            Object[] args = getArgs(command, Arrays.copyOfRange(commandSpec, 1, commandSpec.length));
            command.method.invoke(target, args);
        } catch (ReflectiveOperationException | IllegalArgumentException e) {
            throw new CommandExecutionException(e);
        }
    }

    private Object[] getArgs(CommandDesc command, String[] stringArgs) {
        Iterator<Class<?>> commandArgsIt = command.args.iterator();
        return Arrays.stream(stringArgs)
                                .map(stringValue -> convertTo(commandArgsIt.next(), stringValue))
                                .toArray();
    }

    private Object convertTo(Class<?> argClass, String stringValue) {
        try {
            if (argClass.isPrimitive()) {
                argClass = getPrimitiveWrapper(argClass);
            }
            Method valueOf = argClass.getMethod("valueOf", String.class);
            return valueOf.invoke(null, stringValue);
        } catch (ReflectiveOperationException | IllegalArgumentException e) {
            throw new CommandExecutionException("Error while converting [" + stringValue + "] to [" + argClass.getName() + "]", e);
        }
    }

    private Class<?> getPrimitiveWrapper(Class<?> primitive) {
        if (primitive.equals(int.class)) {
            return Integer.class;
        }
        return null;
    }
}
