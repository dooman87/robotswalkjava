package com.dpokidov.robotswalk.dsl;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A parser which extracts commands and puts them to an executor.
 * <br/>
 * Each command starts from new line and consists of lexemas.
 * Lexema's delimeter is a white space.
 * Supports comments that should be started from '//'
 * <br/>
 * Grammar can be represented as:<br/>
 * COMMAND  ::= NAME ARGLIST<br/>
 * NAME     ::= WORD<br/>
 * ARGLIST  ::= ARG ARGLIST<br/>
 * ARG      ::= WORD<br/>
 * 
 * @author DPokidov
 *
 */
public class Parser {
    private final Logger log = Logger.getLogger(Parser.class.getName());
    private static final String COMMENT = "//";

    private BufferedReader source;
    private CommandExecutor executor;

    /**
     * Creates a new parser.
     * @param source a reader to read input line by line
     * @param executor an executor which interprets commands
     * @throws IllegalArgumentException if source or executor is {@code null} 
     */
    public Parser(BufferedReader source, CommandExecutor executor) {
        if (source == null || executor == null) {
            throw new IllegalArgumentException("Source and executor must not be null");
        }
        this.source = source;
        this.executor = executor;
    }

    /**
     * Starts parser to read lines from source.
     * @throws IOException
     */
    public void start() throws IOException {
        String line = source.readLine();
        while (line != null) {
            int commentStartIdx = line.indexOf(COMMENT);
            if (commentStartIdx > -1) {
                line = line.substring(0, commentStartIdx);
            }
            line = line.trim().replaceAll("\\s+", " ");
            if (line.length() > 0) {
                try {
                    executor.execute(line.split(" "));
                } catch (Throwable t) {
                    log.log(Level.SEVERE, "Error while executing a command", t);
                }
            }
            line = source.readLine();
        }
    }
}
