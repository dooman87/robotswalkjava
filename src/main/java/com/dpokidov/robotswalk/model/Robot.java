package com.dpokidov.robotswalk.model;

import java.io.Serializable;

/**
 * A basic model class of an application.<br/>
 * Represents a current state of a robot including a position and a direction.
 * 
 * @author DPokidov
 */
@SuppressWarnings("serial")
public class Robot implements Serializable {
    private int x = -1;
    private int y = -1;
    private Direction direction;
    private boolean placed;

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public boolean isPlaced() {
        return placed;
    }

    public void placed() {
        this.placed = true;
    }
}
