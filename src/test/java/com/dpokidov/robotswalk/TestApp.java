package com.dpokidov.robotswalk;

import static org.junit.Assert.*;

import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;

/**
 * Simple integration test for application
 * @author DPokidov
 */
public class TestApp {
    private String out = "";

    @Before
    public void setUp() {
        System.setOut(mock(PrintStream.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                if ("println".equals(invocation.getMethod().getName())) {
                    out += invocation.getArguments()[0] + "\n";
                }
                return null;
            }
        }).when(System.out).println(anyString());
    }

    @Test
    public void testWalk() throws Exception {
        App.main(new String[]{getFile("/test.txt")});
        String expectedResult = 
        "0 0 EAST\n" +
        "1 0 EAST\n" +
        "2 0 EAST\n" +
        "3 0 EAST\n" +
        "4 0 EAST\n" +
        "4 0 EAST\n" +
        "4 0 NORTH\n" +
        "4 1 NORTH\n" +
        "4 1 WEST\n" +
        "3 1 WEST\n" +
        "2 1 WEST\n" +
        "1 1 WEST\n" +
        "0 1 WEST\n" +
        "0 1 WEST\n" +
        "0 1 NORTH\n" +
        "0 2 NORTH\n" +
        "0 2 EAST\n" +
        "1 2 EAST\n" +
        "2 2 EAST\n" +
        "3 2 EAST\n" +
        "4 2 EAST\n" +
        "4 2 EAST\n" +
        "4 2 NORTH\n" +
        "4 3 NORTH\n" +
        "4 3 WEST\n" +
        "3 3 WEST\n" +
        "2 3 WEST\n" +
        "1 3 WEST\n" +
        "0 3 WEST\n" +
        "0 3 WEST\n" +
        "0 3 NORTH\n" +
        "0 4 NORTH\n" +
        "0 4 EAST\n" +
        "1 4 EAST\n" +
        "2 4 EAST\n" +
        "3 4 EAST\n" +
        "4 4 EAST\n" +
        "4 4 EAST\n" +
        "4 4 SOUTH\n" +
        "4 3 SOUTH\n";
        assertEquals(expectedResult, out);
    }

    @Test
    public void testWalkNotPlaced() throws Exception {
        App.main(new String[]{getFile("/testNotPlaced.txt")});
        assertEquals("", out);
    }

    /**
     * Remove leading slash on windows /C:/blahblah -> C:/blahblah
     * @param testFileName
     * @return
     */
    private String getFile(String testFileName) {
        String testFile = TestApp.class.getResource(testFileName).getPath();
        if (System.getProperty("os.name").toLowerCase().contains("win")) {
            testFile = testFile.substring(1);
        }
        return testFile;
    }
}
