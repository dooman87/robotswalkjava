package com.dpokidov.robotswalk;

import static org.junit.Assert.*;
import static com.dpokidov.robotswalk.model.Direction.*;

import org.junit.Before;
import org.junit.Test;

import com.dpokidov.robotswalk.model.Direction;
import com.dpokidov.robotswalk.model.Robot;

public class TestRobotController extends TestSetup {
    private Robot robot;
    private RobotController robotController;
    @Before
    public void setUp() {
        this.robot = new Robot();
        this.robotController = new RobotController(robot);
    }

    @Test
    public void testPlace() throws Exception {
        robotController.place(0, 0, NORTH);
        assertEquals(NORTH, robot.getDirection());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test(expected=PositionOutOfBoundsException.class)
    public void testPlaceNegativeX() throws Exception {
        robotController.place(-1, 0, NORTH);
    }

    @Test(expected=PositionOutOfBoundsException.class)
    public void testPlaceNegativeY() throws Exception {
        robotController.place(0, -1, NORTH);
    }

    @Test(expected=IllegalArgumentException.class)
    public void testPlaceNullDirection() throws Exception {
        robotController.place(0, 0, null);
    }

    @Test(expected=PositionOutOfBoundsException.class)
    public void testPlaceOutboundX() throws Exception {
        robotController.place(5, 0, NORTH);
    }

    @Test(expected=PositionOutOfBoundsException.class)
    public void testPlaceOutboundY() throws Exception {
        robotController.place(0, 5, NORTH);
    }

    @Test(expected=RobotNotPlacedException.class)
    public void testMoveBeforePlaced() throws Exception {
        robotController.move();
    }

    @Test
    public void testMoveNorth() throws Exception {
        placeAndMove(NORTH);
        assertRobotState(1, 2, NORTH);
    }

    @Test
    public void testMoveSouth() throws Exception {
        placeAndMove(SOUTH);
        assertRobotState(1, 0, SOUTH);
    }

    @Test
    public void testMoveEast() throws Exception {
        placeAndMove(EAST);
        assertRobotState(2, 1, EAST);
    }

    @Test
    public void testMoveWest() throws Exception {
        placeAndMove(WEST);
        assertRobotState(0, 1, WEST);
    }

    @Test(expected = RobotNotPlacedException.class)
    public void testLeftNotPlaced() throws Exception {
        robotController.left();
    }

    @Test
    public void testLeftNorth() throws Exception {
        placeAndLeft(NORTH);
        assertRobotState(0, 0, WEST);
    }

    @Test
    public void testLeftSouth() throws Exception {
        placeAndLeft(SOUTH);
        assertRobotState(0, 0, EAST);
    }

    @Test
    public void testLeftEast() throws Exception {
        placeAndLeft(EAST);
        assertRobotState(0, 0, NORTH);
    }

    @Test
    public void testLeftWest() throws Exception {
        placeAndLeft(WEST);
        assertRobotState(0, 0, SOUTH);
    }

    @Test(expected = RobotNotPlacedException.class)
    public void testRightNotPlaced() throws Exception {
        robotController.right();
    }

    @Test
    public void testRightWest() throws Exception {
        placeAndRight(WEST);
        assertRobotState(0, 0, NORTH);
    }

    @Test
    public void testRightEast() throws Exception {
        placeAndRight(EAST);
        assertRobotState(0, 0, SOUTH);
    }

    @Test
    public void testRightNorth() throws Exception {
        placeAndRight(NORTH);
        assertRobotState(0, 0, EAST);
    }

    @Test
    public void testRightSouth() throws Exception {
        placeAndRight(SOUTH);
        assertRobotState(0, 0, WEST);
    }

    private void placeAndRight(Direction direction) throws Exception {
        robotController.place(0, 0, direction);
        robotController.right();
    }

    private void placeAndLeft(Direction direction) throws Exception {
        robotController.place(0, 0, direction);
        robotController.left();
    }

    private void placeAndMove(Direction direction) throws Exception {
        robotController.place(1, 1, direction);
        robotController.move();
    }

    public void assertRobotState(int x, int y, Direction direction) {
        assertEquals("X is invalid", x, robot.getX());
        assertEquals("Y is invalid", y, robot.getY());
        assertEquals("Direction is invalid", direction, robot.getDirection());
    }
}
