package com.dpokidov.robotswalk;

import java.util.logging.LogManager;

import org.junit.BeforeClass;

public class TestSetup {
    @BeforeClass
    public static void setupLogger() throws Exception {
        LogManager.getLogManager().readConfiguration(TestSetup.class.getResourceAsStream("/logging-test.properties"));
    }
}
