package com.dpokidov.robotswalk.dsl;

import static org.junit.Assert.*;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.LogManager;

import org.junit.Before;
import org.junit.Test;

import com.dpokidov.robotswalk.TestSetup;

public class TestCommandExecutor extends TestSetup {
    public enum Enum {
        ENUM_ELEMENT
    }
    class Target {
        @Command
        public void command() {
            executionResult = "command executed";
        }

        public void just_method() {
            executionResult = "just method executed";
        }

        @Command
        public void command_with_args(int arg, Enum e) {
            executionResult = "command executed with " + arg + " " + e.name();
        }
    }

    private String executionResult;
    private CommandExecutor commandExecutor;

    @Before
    public void setUp() throws Exception {
        commandExecutor = new CommandExecutor(new Target());
    }

    @Test
    public void testExecuteCommand() throws Exception {
        commandExecutor.execute("command");
        assertEquals("command executed", executionResult);
    }

    @Test(expected = CommandNotFoundException.class)
    public void testNotExistingCommand() throws Exception {
        commandExecutor.execute("nosuchcommand");
    }

    @Test(expected = CommandNotFoundException.class)
    public void testExecuteMethod() throws Exception {
        commandExecutor.execute("just_method");
    }

    @Test
    public void testExecuteCommandWithArgs() throws Exception {
        commandExecutor.execute("command_with_args", "5", "ENUM_ELEMENT");
        assertEquals("command executed with 5 ENUM_ELEMENT", executionResult);
    }

    @Test(expected = CommandExecutionException.class)
    public void testExecuteWithMissArguments() throws Exception {
        commandExecutor.execute("command_with_args", "5");
    }

    @Test(expected = CommandExecutionException.class)
    public void testInvalidTypeArgument() throws Exception {
        commandExecutor.execute("command_with_args", "notInt", "ENUM_ELEMENT");
    }

    @Test
    public void testExecuteCommandUpperCase() throws Exception {
        commandExecutor.execute("COMMAND");
        assertEquals("command executed", executionResult);
    }
}
