package com.dpokidov.robotswalk.dsl;

import static org.junit.Assert.*;

import java.io.BufferedReader;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.dpokidov.robotswalk.TestSetup;

import static org.mockito.Mockito.*;

public class TestParser extends TestSetup {
    private Object[] commandSpec = null;
    private boolean executed = false;
    private BufferedReader source;
    private CommandExecutor executor;
    private String sourceString;
    private Parser parser;

    @Before
    public void setUp() throws Exception {
        this.source = mock(BufferedReader.class);
        when(source.readLine()).then(new Answer<String>() {
            boolean firstTime = true;
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                if (firstTime) {
                    firstTime = false;
                    return sourceString;
                }
                return null;
            }
        });

        this.executor = mock(CommandExecutor.class);
        doAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                if ("execute".equals(invocation.getMethod().getName())) {
                    executed = true;
                    commandSpec = invocation.getArguments();
                }
                return null;
            }
        }).when(executor).execute(anyVararg());
        this.parser = new Parser(source, executor);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParserNullExecutor() throws Exception {
        new Parser(mock(BufferedReader.class), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testParserNullSource() throws Exception {
        new Parser(null, mock(CommandExecutor.class));
    }

    @Test
    public void testParseLine() throws Exception {
        sourceString = "a b";
        parseAndAssert(new String[] {"a", "b"}, true);
    }

    @Test
    public void testEmptyLine() throws Exception {
        sourceString = "";
        parseAndAssert(null, false);
    }

    @Test
    public void testWhiteSpaces() throws Exception {
        sourceString = "    ";
        parseAndAssert(null, false);
    }

    @Test
    public void testCommandWithWhiteSpaces() throws Exception {
        sourceString = "  a   b   ";
        parseAndAssert(new String[] {"a", "b"}, true);
    }

    @Test
    public void testComment() throws Exception {
        sourceString = "a //comment";
        parseAndAssert(new String[] {"a"}, true);
    }

    @Test
    public void testCommentWholeLine() throws Exception {
        sourceString = "//comment";
        parseAndAssert(null, false);
    }

    private void parseAndAssert(String[] expectedCommand, boolean expectedExecuted) throws Exception {
        parser.start();
        Assert.assertArrayEquals("Command is not equals to expected", expectedCommand, commandSpec);
        Assert.assertEquals("Executed flag is not equals to expected", expectedExecuted, executed);
    }
}
