package com.dpokidov.robotswalk.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.dpokidov.robotswalk.TestSetup;
import com.dpokidov.robotswalk.model.Direction;
import com.dpokidov.robotswalk.model.Robot;

public class TestRobot extends TestSetup {
    private Robot robot;

    @Before
    public void setUp() {
        this.robot = new Robot();
    }

    @Test
    public void testInitPosition() throws Exception {
        assertRobotPosition(-1, -1);
    }

    @Test
    public void testInitDirection() throws Exception {
        assertNull(robot.getDirection());
    }

    @Test
    public void testInitPlaced() throws Exception {
        assertFalse(robot.isPlaced());
    }

    @Test
    public void testSetPosition() throws Exception {
        robot.setPosition(1, 1);
        assertRobotPosition(1, 1);
    }

    @Test
    public void testSetDirection() throws Exception {
        robot.setDirection(Direction.SOUTH);
        assertEquals(Direction.SOUTH, robot.getDirection());
    }

    @Test
    public void testSetPlaced() throws Exception {
        robot.placed();
        assertTrue(robot.isPlaced());
    }

    private void assertRobotPosition(int x, int y) {
        assertEquals("X is invalid", x, robot.getX());
        assertEquals("Y is invalid", y, robot.getY());
    }
}
